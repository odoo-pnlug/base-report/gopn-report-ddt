# -*- coding: utf-8 -*-
{
    'name': "GOPN-REPORT-DDT",
    'summary': "DDT Report for Customer",
    'description': "This report has customized for Customer",
    'author': "GOPN Gruppo Odoo PnLug",
    'website': "https://odoo.pnlug.it",
    'category': 'Uncategorized',
    'version': '10.0.0.1',
    'license': 'AGPL-3',

    'depends': [
        'l10n_it_ddt',
        'report'],
    'data': [
        'data/paper_format.xml',
        'reports/report_ddt.xml'],
}
